//
//  ServiceHelper.swift
//  JsonParser
//
//  Created by Anand Yadav on 06/05/20.
//  Copyright © 2020 Anand Yadav. All rights reserved.
//

import UIKit
let baseURL = "https://calendarific.com/api/v2"
let API_KEY = "94dec5d4ac3d7f2544a2ed564d07f80c5db26462"
//https://calendarific.com/api/v2/holidays?api_key=94dec5d4ac3d7f2544a2ed564d07f80c5db26462&country=in&year=2020
//https://calendarific.com/api/v2/languages?api_key=94dec5d4ac3d7f2544a2ed564d07f80c5db26462
//https://calendarific.com/api/v2/countries?api_key=94dec5d4ac3d7f2544a2ed564d07f80c5db26462

public enum APIDetails : String {
    case APIScheme     = "https"
    case APIHost    = "calendarific.com"
    case APIPath     = "/api/v2"
}
enum ServiceHelper {
    case getHolidays(parameters: [String:Any])
    case getLanguages
    case getCountries
    
    var method: HTTPMethod {
        switch self {
        case .getHolidays:
            return .get
        case .getLanguages:
            return .get
        case .getCountries:
            return .get
        }
    }
    
    var path: String {
        switch self {
        case .getHolidays:
            return "/holidays"
        case .getLanguages:
            return "/languages"
        case .getCountries:
            return "/countries"
        }
    }
    
    var urlComponent:URLComponents {
        var components = URLComponents()
        components.scheme = APIDetails.APIScheme.rawValue
        components.host   = APIDetails.APIHost.rawValue
        components.path = APIDetails.APIPath.rawValue + path
        components.queryItems = [URLQueryItem]()
        components.queryItems?.append(URLQueryItem(name: "api_key", value: API_KEY))

        switch self {
        case .getHolidays(let parameters):
            if !parameters.isEmpty {
                for (key, value) in parameters {
                    let queryItem = URLQueryItem(name: key, value: "\(value)")
                    components.queryItems!.append(queryItem)
                }
            }
        default:
            break
//        case .getLanguages(let parameters):
//            if !parameters.isEmpty {
//                for (key, value) in parameters {
//                    let queryItem = URLQueryItem(name: key, value: "\(value)")
//                    components.queryItems!.append(queryItem)
//                }
//            }
//        case .getCountries(let parameters):
//            if !parameters.isEmpty {
//                for (key, value) in parameters {
//                    let queryItem = URLQueryItem(name: key, value: "\(value)")
//                    components.queryItems!.append(queryItem)
//                }
//            }
        }
        return components
    }
    
    func asURLRequest() -> URLRequest {
        var urlRequest = URLRequest(url: urlComponent.url!)
        urlRequest.httpMethod = method.rawValue
        urlRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
        debugPrint(urlRequest)
        return urlRequest
    }
}
public enum HTTPMethod : String {
    case get     = "GET"
    case post    = "POST"
    case put     = "PUT"
    case patch   = "PATCH"
    case delete  = "DELETE"
}

extension Decodable {
    static func decode(with decoder: JSONDecoder = JSONDecoder(), from data: Data) throws -> Self? {
        do {
            decoder.keyDecodingStrategy = .convertFromSnakeCase
            let utf8Data = String(decoding: data, as: UTF8.self).data(using: .utf8)
            return try decoder.decode(Self.self, from: utf8Data!)
        } catch let error {
            print(error)
            throw error
        }
    }
}

enum HTTPError: Error {
    case invalidURL
    case invalidResponse
    case invalidCharacterFound(Character)
}

extension HTTPError: LocalizedError {
    var errorDescription: String? {
        switch self {
        case .invalidURL:
            return NSLocalizedString(
                "A server with the specified hostname could not be found.",
                comment: ""
            )
        case .invalidResponse:
            return NSLocalizedString(
                "Invalid response data",
                comment: ""
            )
        case .invalidCharacterFound(let character):
            let format = NSLocalizedString(
                "Your username can't contain the character '%@'",
                comment: ""
            )
            
            return String(format: format, String(character))
        }
    }
}
