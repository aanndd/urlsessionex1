//
//  ServiceManager.swift
//  MVVM_Example
//
//  Created by Anand Yadav on 24/05/20.
//  Copyright © 2020 Anand Yadav. All rights reserved.
//

import UIKit

class ServiceManager: NSObject {
    static func getHolidays(param: [String:Any], success: @escaping (_ response: Holiday?) -> Void,
                            failure: @escaping (_ error: Error?) -> Void) {
        BaseService.sharedInstance.apiRequest(urlRequest: ServiceHelper.getHolidays(parameters: param).asURLRequest()) { (result) in
            switch result {
            case .success(let data):
                if let canada = try? Holiday.decode(from: data) {
                    success(canada)
                } else {
                    failure(HTTPError.invalidResponse)
                }
            case .failure(let error):
                failure(error)
            }
        }
    }
    
    static func getLanguages(success: @escaping (_ response: Holiday?) -> Void,
                            failure: @escaping (_ error: Error?) -> Void) {
        BaseService.sharedInstance.apiRequest(urlRequest: ServiceHelper.getLanguages.asURLRequest()) { (result) in
            switch result {
            case .success(let data):
                if let canada = try? Holiday.decode(from: data) {
                    success(canada)
                } else {
                    failure(HTTPError.invalidResponse)
                }
            case .failure(let error):
                failure(error)
            }
        }
    }
    
    static func getCountries(success: @escaping (_ response: Holiday?) -> Void,
                            failure: @escaping (_ error: Error?) -> Void) {
        BaseService.sharedInstance.apiRequest(urlRequest: ServiceHelper.getCountries.asURLRequest()) { (result) in
            switch result {
            case .success(let data):
                if let canada = try? Holiday.decode(from: data) {
                    success(canada)
                } else {
                    failure(HTTPError.invalidResponse)
                }
            case .failure(let error):
                failure(error)
            }
        }
    }
}
