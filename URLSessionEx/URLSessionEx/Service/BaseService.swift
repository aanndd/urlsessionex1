//
//  BaseService.swift
//  MVVM_Example
//
//  Created by Anand Yadav on 24/05/20.
//  Copyright © 2020 Anand Yadav. All rights reserved.
//

import UIKit

struct BaseService {
    static let sharedInstance = BaseService()
    
    private let session = URLSession.shared
    
    func apiRequest(urlRequest:URLRequest, completion: @escaping (Result<Data, Error>) -> Void) {
        
        URLSession.shared.dataTask(with: urlRequest) { data, response, error in
            if let _ = error {
                completion(.failure(HTTPError.invalidURL))
                return
            }
            guard let response = response as? HTTPURLResponse, 200 ..< 300 ~= response.statusCode else {
                completion(.failure(HTTPError.invalidURL))
                return
            }
            guard let data = data else {
                completion(.failure(HTTPError.invalidResponse))
                return
            }
            completion(.success(data))
            
        }.resume()
    }
}
