//
//  Holiday.swift
//  URLSessionEx
//
//  Created by Anand Yadav on 31/05/20.
//  Copyright © 2020 Anand Yadav. All rights reserved.
//

import UIKit

// MARK: - Holiday
struct Holiday: Codable {
    let meta: Meta?
    let response: Response?
}

// MARK: - Meta
struct Meta: Codable {
    let code: Int?
}

// MARK: - Response
struct Response: Codable {
    let holidays: [HolidayElement]?
    let url: String?
    let countries: [Country]?
    let languages: [Language]?
}

// MARK: - HolidayElement
struct HolidayElement: Codable {
    let name, holidayDescription: String?
    let country: Country?
    let date: DateClass?
    let type: [String]?
    let locations, states: String?

    enum CodingKeys: String, CodingKey {
        case name
        case type
        case holidayDescription = "description"
        case country, date, locations, states
    }
}

// MARK: - Country
struct Country: Codable {
    let id, name, countryName, iso3166: String?
    let totalHolidays, supportedLanguages: Int?
    let uuid: String?
    
    enum CodingKeys: String, CodingKey {
        case id, name
        case countryName = "country_name"
        case iso3166 = "iso-3166"
        case totalHolidays = "total_holidays"
        case supportedLanguages = "supported_languages"
        case uuid
    }
}

// MARK: - DateClass
struct DateClass: Codable {
    let iso: String?
    let datetime: Datetime?
}

// MARK: - Datetime
struct Datetime: Codable {
    let year, month, day: Int?
}

struct Language: Codable {
    let code, name, nativeName: String?
}

