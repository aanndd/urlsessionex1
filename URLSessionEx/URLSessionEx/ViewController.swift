//
//  ViewController.swift
//  URLSessionEx
//
//  Created by Anand Yadav on 30/05/20.
//  Copyright © 2020 Anand Yadav. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate {

    @IBOutlet var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    var holiday: Holiday?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        searchBar.delegate = self
        tableView.tableHeaderView = searchBar

        // Do any additional setup after loading the view.
        
        
//        ServiceManager.getLanguages(success: { (response) in
//            print(response as Any)
//        }) { (error) in
//            print(error?.localizedDescription as Any)
//        }
//
        ServiceManager.getCountries(success: { (response) in
            print(response as Any)
        }) { (error) in
            print(error?.localizedDescription as Any)
        }
       
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return holiday?.response?.holidays?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "myCell", for: indexPath) as! MyTableViewCell
        cell.title.text = "\(indexPath.row+1). " + (self.holiday?.response?.holidays![indexPath.row].name)!
        cell.date.text = self.holiday?.response?.holidays![indexPath.row].date?.iso
        cell.desc.text = self.holiday?.response?.holidays![indexPath.row].holidayDescription
        return cell
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        ServiceManager.getHolidays(param: ["country":searchBar.text!, "year":"2020"], success: { (response) in
            self.holiday = response
            DispatchQueue.main.async {
                self.title = "\(self.holiday!.response!.holidays!.count)" + " Holiday Found!"
                self.tableView.reloadData()
            }
            print(response as Any)
        }) { (error) in
            print(error?.localizedDescription as Any)
        }
    }
    
}

